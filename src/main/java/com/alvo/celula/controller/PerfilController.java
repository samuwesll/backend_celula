package com.alvo.celula.controller;

import com.alvo.celula.model.dto.PerfilUserDto;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.PerfilService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Api(value = "celula", tags = "perfil")
public class PerfilController {

    @Autowired
    private PerfilService service;

    @GetMapping("v1/perfil")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "buscar todos perfis cadastrados")
    public ResponseEntity<Object> findAllPerfil() {
        List<PerfilUserDto> perfilDto = this.service.buscarPerfil();
        Response<Object> response = new Response<>(
                SucessResponse.builder()
                        .mensage("")
                        .value(perfilDto)
                        .build(),
                HttpStatus.OK.value());
        return ResponseEntity.ok(response);
    }

}
