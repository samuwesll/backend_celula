package com.alvo.celula.controller;

import com.alvo.celula.model.dto.request.RedeRequest;
import com.alvo.celula.model.dto.response.RedeResponse;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.RedeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Api(value = "celulas", tags = "redes")
public class RedeController {

    @Autowired
    private RedeService redeService;

    @GetMapping("v1/rede")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "buscar redes cadastradas")
    public ResponseEntity<Response> listarRedes() {
        List<RedeResponse> redes = redeService.buscarRedes();
        Response<List<RedeResponse>> response = new Response<>(
                SucessResponse.<List<RedeResponse>>builder()
                        .mensage(redes.size() + " registros retornados")
                        .value(redes)
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PostMapping("v1/rede")
    @PreAuthorize(Roles.RET_GERENCIAR)
    @ApiOperation(value = "cadastrar rede")
    public ResponseEntity<Response> cadastrarRede(@RequestBody RedeRequest request) {
        RedeResponse redeResponse = this.redeService.cadastrar(request);
        Response<RedeResponse> response = new Response<>(
                SucessResponse.<RedeResponse>builder()
                        .value(redeResponse)
                        .mensage(redeResponse.getId() + " cadastro com sucesso")
                        .build(),
                HttpStatus.CREATED.value());
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

}
