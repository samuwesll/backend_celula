package com.alvo.celula.controller;

import com.alvo.celula.model.dto.request.UsuarioRequest;
import com.alvo.celula.model.dto.response.UsuarioResponse;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Api(value = "celula", tags = "usu\u00E1rios")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("v1/user/login/{login}")
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation(value = "buscar usu\u00E1rio por login")
    public ResponseEntity<Response<?>> buscarUserPorLogin(@PathVariable(value = "login") String login) {
        UsuarioResponse user = service.buscaPorLogin(login);
        Response<UsuarioResponse> response = new Response(
                SucessResponse.builder()
                        .value(user)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    };

    @GetMapping("v1/user/id/{id}")
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation(value = "buscar usu\u00E1rio por id")
    public ResponseEntity<Response<?>> buscarUserPorId(@PathVariable(value = "id") Long id) {
        UsuarioResponse user = service.buscaPorId(id);
        Response<UsuarioResponse> response = new Response(
                SucessResponse.builder()
                        .value(user)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PostMapping("v1/user")
    @PreAuthorize(Roles.RET_GERENCIAR)
    @ApiOperation(value = "cadastrar usu\u00E1rio")
    public ResponseEntity<Response> cadastrarUsuario(@RequestBody UsuarioRequest usuarioRequest) {
        Long id = this.service.cadastrarUsuario(usuarioRequest);
        Response<Long> response = new Response(
                SucessResponse.builder()
                        .value(id)
                        .mensage("Usuario cadastrado com sucesso")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("v1/user")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "consultar todos os usuarios")
    public ResponseEntity<Response> listarTodos() {
        List<UsuarioResponse> usuario = this.service.listarTodos();
        Response<List<UsuarioResponse>> response = new Response(
                SucessResponse.builder()
                        .value(usuario)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("v1/usuarios/perfil/{idPerfil}")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "consultar todos os supervisores")
    public ResponseEntity<List<UsuarioResponse>> listarSupervisores(@PathVariable("idPerfil") Long idPerfil) {
        List<UsuarioResponse> usuario = this.service.buscarPorPerfil(idPerfil);
        Response<List<UsuarioResponse>> response = new Response(
                SucessResponse.builder()
                        .value(usuario)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("v1/usuarios/rede/{idRede}")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "consultar todos os supervisores")
    public ResponseEntity<List<UsuarioResponse>> listarPorRede(@PathVariable("idRede") Long idRede) {
        List<UsuarioResponse> usuario = this.service.listarPorIdRede(idRede);
        Response<List<UsuarioResponse>> response = new Response(
                SucessResponse.builder()
                        .value(usuario)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
