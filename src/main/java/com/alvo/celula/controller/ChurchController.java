package com.alvo.celula.controller;

import com.alvo.celula.model.dto.ChurchDto;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.ChurchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Api(value = "celula", tags = "igrejas")
public class ChurchController {

    @Autowired
    private ChurchService service;

    @PostMapping("v1/church")
    @PreAuthorize(Roles.RET_GERENCIAR)
    @ApiOperation(value = "cadastrar igreja do Alvo")
    public ResponseEntity<Response> criarChurch(@Valid @RequestBody ChurchDto churchDto) {
        Long id = service.salvarChurch(churchDto);
        Response<Long> response =  new Response(
                SucessResponse.builder()
                        .mensage("Igreja cadastrada com sucesso")
                        .value(id)
                        .build(),
                HttpStatus.OK.value());
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "v1/church/lista", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "listar todas igrejas do Alvo")
    public ResponseEntity<Response<List<ChurchDto>>> listar() {
        List<ChurchDto> churchDtos = service.listarChurch();
        Response<List<ChurchDto>> response = new Response(
                SucessResponse.builder()
                        .value(churchDtos)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
