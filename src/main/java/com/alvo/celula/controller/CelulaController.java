package com.alvo.celula.controller;

import com.alvo.celula.model.dto.response.CelulaDetalheResponse;
import com.alvo.celula.model.dto.response.CelulaResponse;
import com.alvo.celula.model.dto.request.CelulaRequest;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.CelulaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Api(value = "celula", tags = "células")
public class CelulaController {

    @Autowired
    private CelulaService service;

    @GetMapping("v1/celula/{id}")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "Buscar detalhe da celula por id")
    public ResponseEntity<Response<CelulaDetalheResponse>> buscarPorId(@PathVariable(value = "id") Long id) {
        CelulaDetalheResponse celulaDetalhe = service.celulaDetalhe(id);
        Response<CelulaDetalheResponse> response = new Response(
                SucessResponse.builder()
                        .value(celulaDetalhe)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value()
        );
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("v1/celula")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "Listar celulas")
    public ResponseEntity<Response<List<CelulaResponse>>> buscarCelular() {
        List<CelulaResponse> celulaResponses = service.buscarCelulas();
        Response<List<CelulaResponse>> response = new Response(
                SucessResponse.builder()
                        .value(celulaResponses)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK) ;
    }

    @PostMapping("v1/celula")
    @PreAuthorize(Roles.RET_GERENCIAR)
    @ApiOperation(value = "Cadastrar celula")
    public ResponseEntity<Response<Object>>  cadastrar(@RequestBody CelulaRequest celulaRequest) {
        Long id = service.cadastrarCelula(celulaRequest);
        Response<String> response = new Response(
                SucessResponse.builder()
                        .value(id)
                        .mensage("Cadastro realizado com sucesso ")
                        .build(),
                HttpStatus.CREATED.value());
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @DeleteMapping("v1/celula/{id}")
    @PreAuthorize("hasAnyRole('ADMIN')")
    @ApiOperation(value = "Deletar celula")
    public ResponseEntity<Response<Object>> delelar(@PathVariable(value = "id") Long id) {
        this.service.deletarCelula(id);
        Response<Object> response = new Response<>(
                SucessResponse.builder()
                        .mensage("Celula deletada com sucesso")
                        .value("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PatchMapping("v1/celula/associar")
    @PreAuthorize(Roles.RET_GERENCIAR)
    @ApiOperation(value = "Vincular célula ao um usuario co-lider/lider/supervisor")
    public ResponseEntity<Response> associarLider(@RequestParam Long codigoCelula, @RequestParam Long codigoLider, @RequestParam Long codigoPerfil) {
        this.service.associarCelula(codigoCelula, codigoLider, codigoPerfil);
        Response response = new Response(SucessResponse.builder()
                .mensage("Lider vinculado com sucesso!")
                .build(),
                HttpStatus.CREATED.value());
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @GetMapping("v1/celula/login")
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation(value = "Listar celulas pelo login")
    public ResponseEntity<Response<List<CelulaResponse>>> buscarCelularPeloToken() {
        List<CelulaResponse> celulaResponses = service.buscarCelulasPorIdLider();
        Response<List<CelulaResponse>> response = new Response(
                SucessResponse.builder()
                        .value(celulaResponses)
                        .mensage("")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK) ;
    }
}
