package com.alvo.celula.controller;

import com.alvo.celula.model.dto.request.RelatorioCelulaRequest;
import com.alvo.celula.model.dto.request.SearchRelatorio;
import com.alvo.celula.model.dto.response.CargaInicialApp;
import com.alvo.celula.model.dto.response.RelatorioResponse;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.RelatorioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Api(value = "celulas", tags = "relatorios")
public class RelatorioController {

    @Autowired
    private RelatorioService relatorioService;

    @PostMapping("v1/relatorio")
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation(value = "criar relatorio de célula")
    public ResponseEntity<CargaInicialApp> cadastrarRelatorio(@RequestBody RelatorioCelulaRequest request, HttpServletRequest httpRequest) {
        Long id = this.relatorioService.salvar(request);
        CargaInicialApp cargaInicialApp = this.relatorioService.cargaInicial();
        Response<Object> response = new Response(
                SucessResponse.builder()
                        .value(cargaInicialApp)
                        .mensage(id != null ? "Cadastro realizado com sucesso" : "Relatorio do dia, j\u00E1 enviado")
                        .build(),
                HttpStatus.CREATED.value()
        );
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping("v1/relatorio/{id}")
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation(value = "editar relatorio de célula")
    public ResponseEntity<Response> editarRelatorio(@RequestBody RelatorioCelulaRequest request, @PathVariable("id") Long id) {
        this.relatorioService.editar(request, id);
        Response<Object> response = new Response(
                SucessResponse.builder()
                        .value(null)
                        .mensage("Edição realizada com sucesso")
                        .build(),
                HttpStatus.OK.value()
        );
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Gerar relatorio do mês apartir do search")
    @PreAuthorize(Roles.RET_GERENCIAR)
    @GetMapping("v1/relatorio/gerar")
    public ResponseEntity<List<RelatorioResponse>> gerarRelatorio(
            @RequestParam String anoEMesInicio, @RequestParam String anoEMesFim,
            @RequestParam(required=false) Long idSupervisor, @RequestParam(required = false) Boolean flagSupervisor
    ) {
        SearchRelatorio search = new SearchRelatorio(anoEMesInicio, anoEMesFim,idSupervisor, flagSupervisor);
        List<RelatorioResponse> relatorio = this.relatorioService.buscarRelatorios(search);
        Response<Object> response = new Response(
                SucessResponse.builder()
                        .value(relatorio)
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("v1/app/carga-inicial")
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation(value = "Carga inicial para o APP")
    public ResponseEntity<CargaInicialApp> cargaInicial() {
        CargaInicialApp cargaInicialApp = this.relatorioService.cargaInicial();
        Response<CargaInicialApp> response = new Response(
                SucessResponse.builder()
                        .value(cargaInicialApp)
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    };

}
