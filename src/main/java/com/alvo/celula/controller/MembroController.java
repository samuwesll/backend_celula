package com.alvo.celula.controller;

import com.alvo.celula.model.dto.request.MembroRequest;
import com.alvo.celula.model.dto.response.CargaInicialAppMembros;
import com.alvo.celula.model.dto.response.MembroResponse;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.MembroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "celulas", tags = "membros")
@RestController()
@RequestMapping(value = "v1/membro", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class MembroController {

    @Autowired
    private MembroService membroService;

    @PostMapping
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation("cadastrar membros da c\u00E9lula")
    public ResponseEntity<Response<Long>> cadastrar(@RequestBody MembroRequest request) {
        Long idMembro = this.membroService.cadastrar(request);
        Response response = new Response(
                SucessResponse.builder()
                        .mensage("Cadastro realizado com sucesso. ")
                        .value(idMembro)
                        .build(),
                HttpStatus.CREATED.value()
        );
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PatchMapping
    @PreAuthorize(Roles.RET_TODOS)
    @ApiOperation("editar membro da c\u00E9lula")
    public ResponseEntity<Response> editar(@RequestBody MembroRequest request) {
        this.membroService.editar(request);
        Response response = new Response(
                SucessResponse.builder()
                        .mensage("Altera\u00E7\u00E3o realizada com sucesso. ")
                        .build(),
                HttpStatus.CREATED.value()
        );
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation("Listar membros vinculados com a célula")
    @PreAuthorize(Roles.RET_TODOS)
    @GetMapping(value = "celula/{idCelula}")
    public ResponseEntity<Response<List<MembroResponse>>> listarMembros(@PathVariable(value = "idCelula" ) Long idCelula) {
        List<MembroResponse> membros = this.membroService.membrosCelula(idCelula);
        Response<List<MembroResponse>> response = new Response(
                SucessResponse.builder()
                        .value(membros)
                        .build(),
                HttpStatus.OK.value()
        );
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation("Deletar membro através do id informado")
    @DeleteMapping(value = "/{idMembro}")
    @PreAuthorize(Roles.RET_TODOS)
    public ResponseEntity<Response> deletarMembro(@PathVariable(value = "idMembro") Long idMembro) {
        this.membroService.deletar(idMembro);
        Response<Object> response = new Response(
                SucessResponse.builder()
                        .mensage("Cancelamento realizado com sucesso. ")
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation("Buscar membros de c\u00E9lulas pelo login")
    @PreAuthorize(Roles.RET_TODOS)
    @GetMapping(value = "/buscaPorUser")
    public ResponseEntity<Response<List<CargaInicialAppMembros>>> buscarMembrosPorUser() {
        List<CargaInicialAppMembros> cargaInicialAppMembros = this.membroService.carregarMembrosPorUser();
        Response<List<CargaInicialAppMembros>> response = new Response(
                SucessResponse.builder()
                        .value(cargaInicialAppMembros)
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
