package com.alvo.celula.controller;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.response.UsuarioResponse;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.JwtAuthenticationDto;
import com.alvo.celula.security.model.TokenDto;
import com.alvo.celula.security.util.JwtTokenUtil;
import com.alvo.celula.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/auth", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@CrossOrigin(origins = "*")
@Api(value = "celula", tags = "autentica\u00E7\u00E3o")
public class AuthenticationController {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationController.class);
    private static final String TOKEN_HEADER = "Authorization";
    private static final String BEARER_PREFIX = "Bearer ";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService service;

    @PostMapping()
    @ApiOperation(value = "Gerar token do usu\u00E1rio j\u00E1 cadastrado")
    public ResponseEntity<Response<TokenDto>> gerarTokenJwt(@Valid @RequestBody JwtAuthenticationDto authenticationDto) throws AuthenticationException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationDto.getLogin(), authenticationDto.getSenha()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationDto.getLogin());
        String token = jwtTokenUtil.obterToken(userDetails);
        UsuarioResponse usuarioResponse = service.buscaPorLogin(authenticationDto.getLogin());
        Response<TokenDto> response = new Response<TokenDto>(
                SucessResponse.<TokenDto>builder()
                        .value(new TokenDto(token, usuarioResponse))
                        .mensage("Token criado com sucesso")
                        .build(),
                HttpStatus.OK.value());
        return ResponseEntity.ok(response);
    }

    @PostMapping("login")
    @ApiOperation(value = "Gerar acesso para retaguarda")
    public ResponseEntity<Response<TokenDto>> realizarLoginRetaguarda(@Valid @RequestBody JwtAuthenticationDto authenticationDto) throws AuthenticationException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationDto.getLogin(), authenticationDto.getSenha()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String role = authentication.getAuthorities().stream().findFirst().get().getAuthority();
        List<String> role_blocked = Arrays.asList("ROLE_LIDER", "ROLE_COLIDER");

        if (role_blocked.contains(role)) {
            throw new ExcessaoErro("Login sem previlegios para acessar o sistema", HttpStatus.FORBIDDEN);
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationDto.getLogin());
        String token = jwtTokenUtil.obterToken(userDetails);
        UsuarioResponse usuarioResponse = service.buscaPorLogin(authenticationDto.getLogin());
        Response<TokenDto> response = new Response<TokenDto>(
                SucessResponse.<TokenDto>builder()
                        .value(new TokenDto(token, usuarioResponse))
                        .mensage("Token criado com sucesso")
                        .build(),
                HttpStatus.OK.value());
        return ResponseEntity.ok(response);
    }

}
