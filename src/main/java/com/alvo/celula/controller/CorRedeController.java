package com.alvo.celula.controller;

import com.alvo.celula.model.dto.request.CorRedeRequest;
import com.alvo.celula.model.dto.response.CorRedeResponse;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.model.response.SucessResponse;
import com.alvo.celula.security.model.Roles;
import com.alvo.celula.service.CorRedeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
@Api(value = "celula", tags = "cor-redes")
public class CorRedeController {

    @Autowired
    private CorRedeService service;

    @GetMapping("v1/cor")
//    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "buscar todas as cores cadastrada")
    public ResponseEntity<List<CorRedeResponse>> findAllCores() {
        List<CorRedeResponse> cores = service.listarTodos();
        Response<List<CorRedeResponse>> response = new Response(
                SucessResponse.builder()
                        .mensage("")
                        .value(cores)
                        .build(),
                HttpStatus.OK.value());
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PostMapping("v1/cor")
    @PreAuthorize(Roles.RET_CONSULTA)
    @ApiOperation(value = "cadastrar cor")
    public ResponseEntity<Response> cadastrar(@RequestBody CorRedeRequest request) {
        this.service.cadastrarNovaCor(request);
        Response response = new Response(
                SucessResponse.builder()
                        .mensage("Cadastro realizado com sucesso. ")
                        .build(),
                HttpStatus.CREATED.value()
        );
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
