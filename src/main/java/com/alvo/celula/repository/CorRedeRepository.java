package com.alvo.celula.repository;

import com.alvo.celula.model.entity.CorRede;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CorRedeRepository extends JpaRepository<CorRede, Long> {

}
