package com.alvo.celula.repository;

import com.alvo.celula.model.entity.RelatorioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface RelatorioRepository extends JpaRepository<RelatorioEntity, Long> {

    List<RelatorioEntity> findByCelulaVinculoUserLideresId(Long idUser);

    List<RelatorioEntity> findByCelulaIdAndDataRelatorio(Long idCelula, Date data);

}
