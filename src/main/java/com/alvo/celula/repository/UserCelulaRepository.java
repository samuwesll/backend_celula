package com.alvo.celula.repository;

import com.alvo.celula.model.entity.UserCelula;
import com.alvo.celula.model.entity.UserCelulaPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserCelulaRepository extends JpaRepository<UserCelula, UserCelulaPK> {
}
