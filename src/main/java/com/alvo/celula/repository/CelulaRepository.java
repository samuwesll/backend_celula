package com.alvo.celula.repository;

import com.alvo.celula.model.entity.CelulaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

@Repository
public interface CelulaRepository extends JpaRepository<CelulaEntity, Long> {

    List<CelulaEntity> findByVinculoUserIdCdUserAndDataDesativadoIsNull(Long idLider);

    HashSet<CelulaEntity> findByVinculoUserLideresRedeIdAndDataDesativadoIsNull(Long idRede);

}
