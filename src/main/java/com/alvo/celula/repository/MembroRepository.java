package com.alvo.celula.repository;

import com.alvo.celula.model.entity.MembroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MembroRepository extends JpaRepository<MembroEntity, Long> {

    List<MembroEntity> findByCelulaId(Long id);

}
