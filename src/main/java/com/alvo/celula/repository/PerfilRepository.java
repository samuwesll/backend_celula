package com.alvo.celula.repository;

import com.alvo.celula.model.entity.PerfilUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerfilRepository extends JpaRepository<PerfilUser, Long> {
}
