package com.alvo.celula.repository;

import com.alvo.celula.model.dto.request.RedeRequest;
import com.alvo.celula.model.dto.response.RedeResponse;
import com.alvo.celula.model.entity.RedeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RedeRepository extends JpaRepository<RedeEntity, Long> {

}
