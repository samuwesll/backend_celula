package com.alvo.celula.repository;

import com.alvo.celula.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByLogin(String login);

    List<UserEntity> findByPerfilUserId(Long idPerfil);

    List<UserEntity> findByRedeId(Long idRede);

}
