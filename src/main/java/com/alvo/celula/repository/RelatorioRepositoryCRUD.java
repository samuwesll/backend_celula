package com.alvo.celula.repository;

import com.alvo.celula.model.entity.RelatorioEntity;

import java.util.List;

public interface RelatorioRepositoryCRUD {

    List<RelatorioEntity> buscarRelatorioPorParametros(String data, String dataFim, Long idUsuario);

    List<RelatorioEntity> buscarRelatorioPorParametrosERede(String data, String dataFim, Long idRede);

}
