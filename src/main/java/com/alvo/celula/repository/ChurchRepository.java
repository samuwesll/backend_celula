package com.alvo.celula.repository;

import com.alvo.celula.model.entity.ChurchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChurchRepository extends JpaRepository<ChurchEntity, Long> {
}
