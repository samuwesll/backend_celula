package com.alvo.celula.repository.crud;

import com.alvo.celula.model.entity.RelatorioEntity;
import com.alvo.celula.repository.RelatorioRepositoryCRUD;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Slf4j
public class RelatorioRepositoryImplCRUD implements RelatorioRepositoryCRUD {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<RelatorioEntity> buscarRelatorioPorParametros(String data, String dataFim, Long idUsuario) {
        Query query = em.createNativeQuery(this.montarQuery(data, dataFim,idUsuario), RelatorioEntity.class);
        List<RelatorioEntity> relatorio = query.getResultList();
        return relatorio;
    }

    @Override
    public List<RelatorioEntity> buscarRelatorioPorParametrosERede(String data, String dataFim, Long idRede) {
        Query query = em.createNativeQuery(this.montarQueryPorRede(data, dataFim,idRede), RelatorioEntity.class);
        List<RelatorioEntity> relatorio = query.getResultList();
        return relatorio;
    }

    private String montarQuery(String data, String dataFim, Long idUsuario) {
        StringBuilder b = new StringBuilder();

        b.append("select trc.* from tb_relatorio_celula trc ");
        b.append("left outer join tb_lider_celula_pk lcpk on trc.cd_celula = lcpk.cd_celula ");
        b.append("where dt_relatorio >= to_date('" + data +"', 'yyyy-MM') ");
        b.append("and dt_relatorio < to_date('" + dataFim +"', 'yyyy-MM') ");
        b.append("and lcpk.cd_user = " + idUsuario + " ");

        return b.toString();
    }

    private String montarQueryPorRede(String data, String dataFim, Long idRede) {
        StringBuilder b = new StringBuilder();

        b.append("select trc.* from tb_relatorio_celula trc ");
        b.append("left outer join tb_lider_celula_pk lcpk on trc.cd_celula = lcpk.cd_celula ");
        b.append("left outer join tb_user tu on tu.cd_user = lcpk.cd_user ");
        b.append("where dt_relatorio >= to_date('" + data +"', 'yyyy-MM') ");
        b.append("and dt_relatorio < to_date('" + dataFim +"', 'yyyy-MM') ");
        b.append("and tu.cd_rede =" + idRede + " group by 1");

        return b.toString();
    }
}
