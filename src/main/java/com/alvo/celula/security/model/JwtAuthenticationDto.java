package com.alvo.celula.security.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
public class JwtAuthenticationDto {

    @NotEmpty(message = "Login não pode ser vazio.")
    private String login;

    @NotEmpty(message = "Senha não pode ser vazia.")
    private String senha;

}
