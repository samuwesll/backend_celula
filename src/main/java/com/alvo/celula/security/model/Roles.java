package com.alvo.celula.security.model;

public class Roles {

    public static final String ADMIN = "hasAnyRole('ADMIN')";
    public static final String RET_CONSULTA = "hasAnyRole('ADMIN', 'DIAC', 'PASTOR', 'BISPO', 'LIDER', 'SUPER')";
    public static final String RET_GERENCIAR = "hasAnyRole('ADMIN', 'DIAC', 'PASTOR', 'BISPO', 'SUPER')";
    public static final String RET_TODOS = "hasAnyRole('ADMIN', 'DIAC', 'PASTOR', 'BISPO', 'LIDER', 'SUPER', 'COLIDER')";

    public Roles() {
    }
}
