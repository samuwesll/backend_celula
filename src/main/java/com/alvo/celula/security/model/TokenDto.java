package com.alvo.celula.security.model;

import com.alvo.celula.model.dto.response.UsuarioResponse;
import lombok.Data;

@Data
public class TokenDto {

    private String token;

    private UsuarioResponse user;

    public TokenDto() {
    }

    public TokenDto(String token, UsuarioResponse usuario) {
        this.token = token;
        this.user = usuario;
    }

}
