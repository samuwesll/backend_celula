package com.alvo.celula.security;

import com.alvo.celula.model.entity.UserEntity;
import com.alvo.celula.model.enums.PerfilEnum;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public class JwtUserFactory {

    public JwtUserFactory() {
    }

    public static JwtUser create(UserEntity usuario) {
        return new JwtUser(usuario.getId(), usuario.getLogin(), usuario.getSenha(),
                mapToGrantedAuthorities(usuario.getPerfilUser().getId()));
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(Long id) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(PerfilEnum.buscarRoles(id)));
        return authorities;
    }
}
