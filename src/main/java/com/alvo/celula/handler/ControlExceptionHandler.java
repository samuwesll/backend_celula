package com.alvo.celula.handler;

import com.alvo.celula.model.response.ErrorResponse;
import com.alvo.celula.model.response.Response;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ControlExceptionHandler {

    @ExceptionHandler(value = {BadCredentialsException.class})
    protected ResponseEntity<Object> handleBadCredentialsException(RuntimeException ex, WebRequest request) {
        ErrorResponse error = new ErrorResponse(HttpStatus.UNAUTHORIZED.name(), "Login ou senha invalido.");
        Response<Object> response = new Response<>(Arrays.asList(error), 401);

        return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {ExpiredJwtException.class})
    protected ResponseEntity<Object> handleConflict(ExpiredJwtException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.name(), "Token expirado.");
        Response<Object> response = new Response<>(Arrays.asList(errorResponse), HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { RuntimeException.class, IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        ErrorResponse erro = new ErrorResponse(ex.getLocalizedMessage(), ex.getMessage());
        Response<Object> response = new Response<>(Arrays.asList(erro), HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<Object> validationError(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        List<String> fieldErrorDtos = fieldErrors.stream()
                .map(f -> f.getField().concat(":").concat(f.getDefaultMessage())).map(String::new)
                .collect(Collectors.toList());
        List<ErrorResponse> erros = (List<ErrorResponse>) fieldErrors.stream().map(e -> new ErrorResponse(e.getField() + ": " + e.getDefaultMessage(),
                "Erro de validacao no payload."));
        Response<Object> response = new Response<>(erros, HttpStatus.BAD_REQUEST.value());
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ExcessaoErro.class})
    public ResponseEntity<Object> handle(ExcessaoErro erro) {
        String error = ExcessaoErro.gerarDescricaoException(erro);
        ErrorResponse errorResponse = new ErrorResponse(error, erro.getLocalizedMessage());
        List<ErrorResponse> listaErros = Arrays.asList(errorResponse);
        Response<Object> response = new Response<>(listaErros, erro.getHttpStatus().value());
        return new ResponseEntity<Object>(response, new HttpHeaders(), erro.getHttpStatus());
    }
}
