package com.alvo.celula.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.util.function.Supplier;

@Data
@EqualsAndHashCode(callSuper=false)
@ToString
public class ExcessaoErro extends RuntimeException {

    private static  final String MENSAGEM_NULL = "NULL";
    private static final String MENSAGEM_EXCLUSIVA = "EXCLUSIVA";
    private static final String TIMED_OUT = "TIMED OUT";
    private HttpStatus httpStatus;
    private Object object;

    public ExcessaoErro(String msg) {
        super(msg);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }

    public ExcessaoErro(String msg, HttpStatus status) {
        super(msg);
        this.httpStatus = status;
    }

    public ExcessaoErro(String msg, Throwable ex, HttpStatus status) {
        super(msg, ex);
        this.httpStatus = status;
    }

    @Override
    public synchronized Throwable initCause(Throwable cause) {
        return super.initCause(cause);
    }

    public static String gerarDescricaoException(Throwable e) {
        if (e.getCause() != null) {
            return "TRACE: Exceção["
                    .concat(e.getCause().toString())
                    .concat("][Classe ")
                    .concat(e.getCause().getStackTrace()[0].getClassName())
                    .concat(", metodo: ")
                    .concat(e.getCause().getStackTrace()[0].getMethodName())
                    .concat(", linha: ")
                    .concat(String.valueOf(e.getCause().getStackTrace()[0]
                            .getLineNumber()))
                    .concat("] "+ (e.getCause().getMessage() != null ?
                            " MensagemWS:{"+e.getCause().getMessage().substring(0, e.getCause().getMessage().length() > 500 ? 500 : e.getCause().getMessage().length()).concat("}")
                            :""));
        }

        return "TRACE: Exceção["
                .concat(e.toString())
                .concat("][Classe ")
                .concat(e.getStackTrace()[0].getClassName())
                .concat(", metodo: ")
                .concat(e.getStackTrace()[0].getMethodName())
                .concat(", linha: ")
                .concat(String.valueOf(e.getStackTrace()[0]
                        .getLineNumber()))
                .concat("] " + (e.getMessage() != null ?  " MensagemWS:{"+e.getMessage().substring(
                        0,
                        e.getMessage().length() > 500 ? 500 : e
                                .getMessage().length()).concat("}") :""));


    }
}
