package com.alvo.celula.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response<T> {

    private SucessResponse<T> sucesso;
    private List<ErrorResponse> errors;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
    private Date timestamp;
    private Integer status;

    public Response() {
    }

    public Response(SucessResponse<T> data, Integer status) {
        this.timestamp = new Date();
        this.sucesso = data;
        this.status = status;
        this.errors = null;
    }

    public Response(List<ErrorResponse> errors, Integer status) {
        this.timestamp = new Date();
        this.errors = errors;
        this.status = status;
    }

    public List<ErrorResponse> getErrors() {
        if (this.errors == null) {
            this.errors = new ArrayList<ErrorResponse>();
        }
        return errors;
    }
}
