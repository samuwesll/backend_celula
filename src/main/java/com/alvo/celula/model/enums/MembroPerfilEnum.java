package com.alvo.celula.model.enums;

public enum MembroPerfilEnum {

    CRIANCA("CRIANÇA"),
    ADULTO("ADULTO");

    private String nome;

    MembroPerfilEnum(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public static String buscarPerfil(String nome) {
        String nomePerfil = null;
        for (MembroPerfilEnum perfil : MembroPerfilEnum.values()) {
            if (perfil.nome.equals(nome)) {
                nomePerfil = nome;
            }
        }
        return nomePerfil;
    }
}
