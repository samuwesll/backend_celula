package com.alvo.celula.model.enums;

public enum PerfilEnum {

    ADMIN("ROLE_ADMIN", 1l),
    BISPO("ROLE_BISPO", 2l),
    PASTOR("ROLE_PASTOR", 3l),
    DIACONO("ROLE_DIAC", 4l),
    LIDER("ROLE_LIDER", 5l),
    COOLIDER("ROLE_COLIDER", 6l),
    SUPERVISOR("ROLE_SUPER", 7l);

    private String role;
    private Long id;

    PerfilEnum(String role, Long id) {
        this.role = role;
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public Long getId() {
        return id;
    }

    public static String buscarRoles(Long id) {
        for (PerfilEnum perfil : PerfilEnum.values()) {
            if (perfil.getId().equals(id)) {
                return perfil.getRole();
            }
        }

        return null;
    }

}
