package com.alvo.celula.model.enums;

public enum DiaSemanaEnum {

    DOMINGO(1),
    SEGUNDA(2),
    TERCA(3),
    QUARTA(4),
    QUINTA(5),
    SEXTA(6),
    SABADO(7);

    private Integer dia;

    DiaSemanaEnum(Integer dia) {
        this.dia = dia;
    }

    public Integer getDia() {
        return dia;
    }

    public static DiaSemanaEnum buscarEnum(Integer dia) {
        for (DiaSemanaEnum d : DiaSemanaEnum.values()) {
            if (d.dia == dia) {return d;}
        }
        throw new IllegalArgumentException("codigo invalido");
    }
}
