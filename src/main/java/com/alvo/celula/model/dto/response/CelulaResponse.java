package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.dto.EnderecoDto;
import com.alvo.celula.model.dto.response.UsuarioResponse;
import com.alvo.celula.model.entity.CelulaEntity;
import com.alvo.celula.model.entity.UserCelula;
import com.alvo.celula.model.entity.UserEntity;
import com.alvo.celula.model.enums.DiaSemanaEnum;
import com.alvo.celula.model.enums.PerfilEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class CelulaResponse {

    private Long id;

    private String nome;

    @Enumerated()
    private DiaSemanaEnum dia;

    @Temporal(TemporalType.TIME)
    private String horario;

    private List<UsuarioResponse> lideres;

    private EnderecoDto endereco;

    private Date dataCadastro;

    public CelulaResponse(CelulaEntity celulaEntity) {
        this.id = celulaEntity.getId();
        this.nome = celulaEntity.getNome();
        this.horario = celulaEntity.getHorario();
        this.dia = DiaSemanaEnum.buscarEnum(celulaEntity.getDiaSemana());
        this.lideres = new ArrayList<>();
        this.dataCadastro = celulaEntity.getDataCadastrado();
        this.endereco = EnderecoDto.builder()
                .cep(celulaEntity.getCep())
                .logradouro(celulaEntity.getLogradouro())
                .numero(celulaEntity.getNumero())
                .bairro(celulaEntity.getBairro())
                .complemento(celulaEntity.getComplemento())
                .estado(celulaEntity.getEstado())
                .uf(celulaEntity.getUf())
                .build();
        for (UserCelula lider : celulaEntity.getVinculoUser()) {
            if (lider.getPerfil().getId().equals(PerfilEnum.LIDER.getId())) {
                this.lideres.add(new UsuarioResponse(lider.getLideres()));
            }
        }

    }
}
