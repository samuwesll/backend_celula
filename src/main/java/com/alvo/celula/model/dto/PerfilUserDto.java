package com.alvo.celula.model.dto;

import com.alvo.celula.model.entity.PerfilUser;
import com.alvo.celula.model.enums.PerfilEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PerfilUserDto {

    private Long id;

    private String cargo;

    public PerfilUserDto(PerfilUser perfil) {
        this.id = perfil.getId();
        this.cargo = perfil.getDescricao();
    }
}
