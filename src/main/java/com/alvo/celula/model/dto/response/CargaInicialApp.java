package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.dto.CargaInicialAppItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class CargaInicialApp {

    List<CargaInicialAppItem> celulas;

    public CargaInicialApp() {
        this.celulas = new ArrayList<>();
    }
}
