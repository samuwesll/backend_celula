package com.alvo.celula.model.dto.request;

import com.alvo.celula.model.enums.MembroPerfilEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MembroRequest {

    private Long id;

    private String apelido;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private String dataNascimento;

    private String nomeCompleto;

    private String nmCelular;

    private Long celulaId;

    @Enumerated()
    private MembroPerfilEnum perfil;

}
