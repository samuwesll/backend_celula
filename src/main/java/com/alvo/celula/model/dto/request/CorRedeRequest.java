package com.alvo.celula.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor
public class CorRedeRequest {

    @NotBlank
    private String nomeCor;

    @NotBlank
    private String stileCor;

}
