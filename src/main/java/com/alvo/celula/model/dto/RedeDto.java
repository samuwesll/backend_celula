package com.alvo.celula.model.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class RedeDto {

    @NotBlank
    private Long id;

    @NotBlank
    private String cor;

    @NotBlank
    private String pastores;

    @NotBlank
    private String stiloCor;
}
