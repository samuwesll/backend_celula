package com.alvo.celula.model.dto.request;

import com.alvo.celula.model.dto.EnderecoDto;
import com.alvo.celula.model.enums.DiaSemanaEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.List;

@Data
@NoArgsConstructor
public class CelulaRequest {

    private String nome;

    @Enumerated()
    private DiaSemanaEnum dia;

    @Temporal(TemporalType.TIME)
    private String horario;

    private EnderecoDto endereco;

    private List<Long> idLideres;

    private List<Long> idSupers;

}
