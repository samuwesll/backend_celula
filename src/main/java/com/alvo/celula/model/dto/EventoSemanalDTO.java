package com.alvo.celula.model.dto;

import com.alvo.celula.model.entity.EventoSemanal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EventoSemanalDTO {

    private Long diaSemana;

    private String evento;

    private String horario;

    private String nome;

    public EventoSemanalDTO(EventoSemanal entity) {
        this.diaSemana = entity.getDiaSemana();
        this.evento = entity.getEvento();
        this.horario = entity.getHorario();
        this.nome = entity.getNome();
    }
}
