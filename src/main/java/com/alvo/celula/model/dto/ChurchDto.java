package com.alvo.celula.model.dto;

import com.alvo.celula.model.entity.ChurchEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@Builder
public class ChurchDto {

    private Long id;

    @NotBlank(message = "Nome da igreja não pode null ou em branco")
    private String nome;

    @NotBlank(message = "Estado da igreja não pode null ou em branco")
    private String estado;

    @NotBlank(message = "UF não pode null ou em branco")
    private String uf;

    @NotBlank(message = "Cidade não pode null ou em branco")
    private String cidade;

    private List<EventoSemanalDTO> eventoSemanal;

    public ChurchDto(ChurchEntity churchEntity) {
        this.id = churchEntity.getId();
        this.nome = churchEntity.getNome();
        this.estado = churchEntity.getEstado();
        this.uf = churchEntity.getSgUf();
        this.cidade = churchEntity.getCidade();
        this.eventoSemanal = churchEntity.getEventoSemanal().stream().map(v -> new EventoSemanalDTO(v)).collect(Collectors.toList());
    }
}
