package com.alvo.celula.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RedeRequest {

    @NotBlank
    private String cor;

    @NotBlank
    private Long idIgreja;

    private String pastores;

    private Long idCor;


}
