package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.dto.ChurchDto;
import com.alvo.celula.model.dto.ContatoDto;
import com.alvo.celula.model.dto.PerfilUserDto;
import com.alvo.celula.model.dto.RedeDto;
import com.alvo.celula.model.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class UsuarioResponse {

    private Long id;

    private String nomeCompleto;

    private String apelido;

    private String login;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date dataCadastro;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date dataNascimento;

    private RedeDto rede;

    private ChurchDto igreja;

    private PerfilUserDto perfil;

    private ContatoDto contato;

    public UsuarioResponse(UserEntity usuario) {
        if (usuario == null) {
            return;
        }
        this.setId(usuario.getId());
        this.apelido = usuario.getApelido();
        this.setNomeCompleto(usuario.getNome());
        this.setLogin(usuario.getLogin());
        this.setDataCadastro(usuario.getDataCadastrado());
        this.setDataNascimento(usuario.getDataNascimento());
        if (usuario.getRede() != null) {
            this.rede = RedeDto.builder()
                    .id(usuario.getRede().getId())
                    .cor(usuario.getRede().getCorRede().getCorRede())
                    .pastores(usuario.getRede().getPastores())
                    .stiloCor(usuario.getRede().getCorRede().getStiloCor())
                    .build();
            this.igreja = new ChurchDto(usuario.getRede().getChurch());
        }
        this.perfil = PerfilUserDto.builder()
                .id(usuario.getPerfilUser().getId())
                .cargo(usuario.getPerfilUser().getDescricao())
                .build();
        this.contato = ContatoDto.builder()
                .celular(usuario.getCelular())
                .email(usuario.getEmail())
                .build();
    }

}
