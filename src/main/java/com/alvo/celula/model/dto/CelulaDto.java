package com.alvo.celula.model.dto;

import com.alvo.celula.model.entity.CelulaEntity;
import com.alvo.celula.model.enums.DiaSemanaEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Data
@NoArgsConstructor
public class CelulaDto {

    private Long id;

    private String nome;

    @Enumerated()
    private DiaSemanaEnum dia;

    @Temporal(TemporalType.TIME)
    private String horario;

    private EnderecoDto endereco;

    private RedeDto rede;

    public CelulaDto(CelulaEntity entity) {
        this.id = entity.getId();
        this.nome = entity.getNome();
        this.horario = entity.getHorario();
        this.dia = DiaSemanaEnum.buscarEnum(entity.getDiaSemana());
        this.endereco = EnderecoDto.builder()
                .cep(entity.getCep())
                .logradouro(entity.getLogradouro())
                .numero(entity.getNumero())
                .bairro(entity.getBairro())
                .complemento(entity.getComplemento())
                .estado(entity.getEstado())
                .uf(entity.getUf())
                .build();
        this.rede = RedeDto.builder()
                .id(entity.getVinculoUser().get(0).getLideres().getRede().getId())
                .cor(entity.getVinculoUser().get(0).getLideres().getRede().getCorRede().getCorRede())
                .pastores(entity.getVinculoUser().get(0).getLideres().getRede().getPastores())
                .stiloCor(entity.getVinculoUser().get(0).getLideres().getRede().getCorRede().getStiloCor())
                .build();
    }
}
