package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.entity.MembroEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MembroResponse {

    private Long id;

    private String apelido;

    private String dataNacimento;

    private String nomeCompleto;

    private String nmCelular;

    private String perfil;

    public static MembroResponse converteEntityParaResponse(MembroEntity entity) {
        MembroResponse membroResponse = MembroResponse.builder()
                .id(entity.getId())
                .apelido(entity.getApelido())
                .dataNacimento(entity.getDataNascimento().toString())
                .nomeCompleto(entity.getNomeCompleto())
                .nmCelular(entity.getCelular())
                .perfil(entity.getPerfil())
                .build();
        return membroResponse;
    }

}
