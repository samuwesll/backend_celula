package com.alvo.celula.model.dto;

import com.alvo.celula.model.dto.response.RelatorioDiaResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
public class CargaInicialAppItem {

    private String nomeCelula;

    private Long idCelula;

    private List<RelatorioMes> meses;

    public CargaInicialAppItem() {
        this.meses = new ArrayList<>();
    }
}
