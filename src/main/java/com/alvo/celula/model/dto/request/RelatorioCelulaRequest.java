package com.alvo.celula.model.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelatorioCelulaRequest {

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private String dataRelatorio;

    private Long idCelula;

    private Long idLider;

    private Double kgAmor;

    private Float oferta;

    private List<Long> idsMembros;

    private Integer visitantes;

    private String observaao;

}
