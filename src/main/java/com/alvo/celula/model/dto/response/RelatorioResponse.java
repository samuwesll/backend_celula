package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.entity.CelulaEntity;
import com.alvo.celula.model.entity.RelatorioEntity;
import com.alvo.celula.model.entity.UserCelula;
import com.alvo.celula.model.entity.UserEntity;
import com.alvo.celula.model.enums.PerfilEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RelatorioResponse {

    private String celula;

    private String lider;

    private Integer membros;

    private Integer visitante;

    private Double ofertas;

    private Double quilos;

    private Boolean status;

    private List<RelatorioDiaResponse> diasRelatorio;

    public RelatorioResponse(CelulaEntity entity) {
        this.celula = entity.getNome();

        this.membros = 0;
        this.visitante = 0;
        this.ofertas = 0.0;
        this.quilos = 0.0;
        this.status = false;
        this.diasRelatorio = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        for (UserCelula lider : entity.getVinculoUser()) {
            if (lider.getPerfil().getId().equals(PerfilEnum.LIDER.getId())) {
                if (builder.length() > 1) {
                    builder.append(" e ");
                    builder.append(Arrays.stream(lider.getLideres().getNome().split(" ")).collect(Collectors.toList()).get(0));
                } else {
                    builder.append(Arrays.stream(lider.getLideres().getNome().split(" ")).collect(Collectors.toList()).get(0));
                }
            }
        }
        this.setLider(builder.toString());
    }
}
