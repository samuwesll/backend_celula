package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.dto.CelulaDto;
import com.alvo.celula.model.dto.PerfilUserDto;
import com.alvo.celula.model.dto.UsuarioDto;
import com.alvo.celula.model.entity.CelulaEntity;
import com.alvo.celula.model.entity.MembroEntity;
import com.alvo.celula.model.entity.UserCelula;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CelulaDetalheResponse {

    private CelulaDto celula;

    private List<MembroResponse> membros;

    private List<UsuarioDto> usuariosAssociados;

    public CelulaDetalheResponse() {
        this.membros = new ArrayList<>();
        this.usuariosAssociados = new ArrayList<>();
    }

    public CelulaDetalheResponse(CelulaEntity celulaEntity, List<MembroEntity> membrosEntities, List<UserCelula> vinculoUser) {
        this.celula = new CelulaDto(celulaEntity);
        this.membros = new ArrayList<>();
        this.usuariosAssociados = new ArrayList<>();

        if (!membrosEntities.isEmpty()) {
            for (MembroEntity membro : membrosEntities) {
                this.membros.add(MembroResponse.converteEntityParaResponse(membro));
            }
        }

        if (!vinculoUser.isEmpty()) {
            for(UserCelula user : vinculoUser) {
                this.usuariosAssociados.add(UsuarioDto.builder()
                        .id(user.getLideres().getId())
                        .nomeCompleto(user.getLideres().getNome())
                        .apelido(user.getLideres().getApelido())
                        .perfilUsuario(new PerfilUserDto(user.getLideres().getPerfilUser()))
                        .papelNaCelula(new PerfilUserDto(user.getPerfil()))
                        .build());
            }
        }
    }
}
