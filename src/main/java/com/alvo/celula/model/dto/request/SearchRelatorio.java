package com.alvo.celula.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SearchRelatorio {

    private String data;

    private String dataFim;

    private Long idSupervisor;

    private Boolean flagSupervisor;

}
