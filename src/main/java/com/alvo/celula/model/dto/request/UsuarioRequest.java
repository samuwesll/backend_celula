package com.alvo.celula.model.dto.request;

import com.alvo.celula.model.dto.ContatoDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class UsuarioRequest {

    private String nomeCompleto;

    private String apelido;

    private String login;

    private String senha;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private String dataNascimento;

    private Long idRede;

    private Long idPerfil;

    private ContatoDto contato;

}
