package com.alvo.celula.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CargaInicialAppMembros {

    private Long idCelula;

    private List<MembroResponse> membros;

}
