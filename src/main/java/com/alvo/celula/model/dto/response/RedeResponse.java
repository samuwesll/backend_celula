package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.dto.ChurchDto;
import com.alvo.celula.model.entity.RedeEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RedeResponse {

    private Long id;

    private String cor;

    private ChurchDto igreja;

    private String pastores;

    public RedeResponse(RedeEntity entity) {
        this.id = entity.getId();
        this.cor = entity.getCor();
        this.pastores = entity.getPastores();
        this.igreja = ChurchDto.builder()
                .id(entity.getChurch().getId())
                .uf(entity.getChurch().getSgUf())
                .cidade(entity.getChurch().getCidade())
                .estado(entity.getChurch().getEstado())
                .nome(entity.getChurch().getNome())
                .build();
    }
}
