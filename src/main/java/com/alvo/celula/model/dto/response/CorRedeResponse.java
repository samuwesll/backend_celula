package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.entity.CorRede;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CorRedeResponse {

    private Long id;

    private String cor;

    private String stiloCor;

    public CorRedeResponse(CorRede entity) {
        this.id = entity.getId();
        this.cor = entity.getCorRede();
        this.stiloCor = entity.getStiloCor();
    }
}
