package com.alvo.celula.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ContatoDto {

    private String email;

    private String celular;

}
