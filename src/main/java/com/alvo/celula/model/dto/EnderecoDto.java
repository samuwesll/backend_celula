package com.alvo.celula.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EnderecoDto {

    private String cep;

    private String logradouro;

    private String numero;

    private String complemento;

    private String bairro;

    private String estado;

    private String uf;

}
