package com.alvo.celula.model.dto.response;

import com.alvo.celula.model.entity.RelatorioEntity;
import com.alvo.celula.model.entity.UserCelula;
import com.alvo.celula.model.enums.PerfilEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RelatorioDiaResponse {

    private Long id;

    private Integer membros;

    private Integer visitante;

    private Float ofertas;

    private Double quilos;

    private Boolean status;

    private Integer nrSemana;

    private String data;

    private String observacao;

    private List<Long> idsMembros;

    public RelatorioDiaResponse(RelatorioEntity entity, Integer nrSemana) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.id = entity.getId();
        this.ofertas = entity.getOferta();
        this.quilos = entity.getKgAmor();
        this.data = dateFormat.format(entity.getDataRelatorio());
//        this.data = entity.getDataRelatorio().toString();
        this.visitante = entity.getVisitantes();
        this.nrSemana = nrSemana + 1;
        this.status = true;
        this.observacao = entity.getObservacao();
        this.idsMembros = new ArrayList<>();
        String[] split = entity.getIdsMembros().split(",");
        try {
            for (String v : split) {
                String result = v.replaceAll("\\s+","");
                idsMembros.add(Long.parseLong(result));
            }
        } catch (Exception e) {}
        if (this.idsMembros.isEmpty()) {
            this.membros = 0;
        } else {
            this.membros = Math.toIntExact(Arrays.stream(entity.getIdsMembros().split(",")).count());
        }

    }

    public RelatorioDiaResponse(Date data, Integer nrSemana) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.data = dateFormat.format(data);
        this.observacao = "Relatorio pendente";
        this.status = false;
        this.ofertas = 0f;
        this.quilos = 0.0;
        this.visitante = 0;
        this.membros = 0;
        this.nrSemana = nrSemana + 1;
    }
}
