package com.alvo.celula.model.dto;

import com.alvo.celula.model.dto.response.RelatorioDiaResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class RelatorioMes {

    private String mesAno;

    private Integer membros;

    private Integer visitante;

    private Double ofertas;

    private Double quilos;

    private Boolean status;

    private List<RelatorioDiaResponse> dias;

    public RelatorioMes(List<RelatorioDiaResponse> dias) {
        this.dias = dias;
        this.ofertas = dias.stream().map(r -> r.getOfertas()).mapToDouble(v -> v.doubleValue()).sum();
        this.quilos = dias.stream().map(q -> q.getQuilos()).mapToDouble(v -> v.doubleValue()).sum();
        this.visitante = dias.stream().map(r -> r.getVisitante()).mapToInt(v -> v.intValue()).sum();
        this.membros = dias.stream().map(r -> r.getMembros()).max(Comparator.comparing(v -> v)).orElse(0);
        this.status = false;
    }
}
