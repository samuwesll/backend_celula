package com.alvo.celula.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class UsuarioDto {

    private Long id;

    private String nomeCompleto;

    private String apelido;

    private PerfilUserDto perfilUsuario;

    private PerfilUserDto papelNaCelula;

}
