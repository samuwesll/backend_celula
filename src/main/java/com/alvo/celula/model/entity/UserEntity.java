package com.alvo.celula.model.entity;

import com.alvo.celula.model.dto.request.UsuarioRequest;
import com.alvo.celula.util.SenhaUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_user")
public class UserEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_user")
    private Long id;

    @Column(name = "nm_user", nullable = false)
    private String nome;

    @Column(name = "ds_senha", nullable = false)
    private String senha;

    @Column(name = "ds_login", nullable = false, unique = true)
    private String login;

    @Column(name = "ds_apelido", nullable = false)
    private String apelido;

    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dataCadastrado;

    @Column(name = "dt_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cd_rede")
    private RedeEntity rede;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cd_perfil")
    private PerfilUser perfilUser;

    @Column(name = "ds_email")
    private String email;

    @Column(name = "nm_celular")
    private String celular;

    public UserEntity(UsuarioRequest usuarioRequest) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        this.nome = usuarioRequest.getNomeCompleto();
        this.login = usuarioRequest.getLogin();
        this.apelido = usuarioRequest.getApelido();
        this.senha = SenhaUtil.gerarBCrypt(usuarioRequest.getSenha());
        this.dataCadastrado = new Date();
        this.celular = usuarioRequest.getContato().getCelular();
        this.email = usuarioRequest.getContato().getEmail();
        try {
            this.dataNascimento = formato.parse(usuarioRequest.getDataNascimento());
        } catch (ParseException e) {
            this.dataNascimento = new Date();
        }
    }

}
