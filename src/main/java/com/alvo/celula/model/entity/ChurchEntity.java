package com.alvo.celula.model.entity;

import com.alvo.celula.model.dto.ChurchDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_church")
public class ChurchEntity implements Serializable {

    @Id
    @Column(name = "cd_church")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nm_church", nullable = false)
    private String nome;

    @Column(name = "sg_uf", nullable = false, length = 2)
    private String sgUf;

    @Column(name = "estado", nullable = false)
    private String estado;

    @Column(name = "cidade", nullable = false)
    private String cidade;

    @OneToMany(mappedBy = "igreja")
    private List<EventoSemanal> eventoSemanal;

    public ChurchEntity(ChurchDto churchDto) {
        this.nome = churchDto.getNome();
        this.sgUf = churchDto.getUf();
        this.estado = churchDto.getEstado();
        this.cidade = churchDto.getCidade();
    }
}
