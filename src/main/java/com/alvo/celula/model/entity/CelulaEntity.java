package com.alvo.celula.model.entity;

import com.alvo.celula.model.dto.request.CelulaRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_celula")
public class CelulaEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "cd_celula")
    private Long id;

    @Column(name = "nm_celula", nullable = false)
    private String nome;

    @Column(name = "dia_semana", nullable = false)
    private Integer diaSemana;

    @Column(name = "hr_celula")
    private String horario;

    @Column(name = "dt_cadastro", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataCadastrado;

    @Column(name = "dt_atualizacao", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataAtualizacao;

    @Column(name = "dt_desativado")
    @Temporal(TemporalType.DATE)
    private Date dataDesativado;

    @Column(name = "nm_cep")
    private String cep;

    @Column(name = "ds_endereco")
    private String logradouro;

    @Column(name = "nm_endereco")
    private String numero;

    @Column(name = "ds_complemento")
    private String complemento;

    @Column(name = "ds_bairro")
    private String bairro;

    @Column(name = "ds_estado")
    private String estado;

    @Column(name = "sg_uf")
    private String uf;

    @OneToMany(mappedBy = "celula", cascade = CascadeType.ALL)
    private List<UserCelula> vinculoUser;

    public CelulaEntity(CelulaRequest celulaRequest) {
        this.nome = celulaRequest.getNome();
        this.diaSemana = celulaRequest.getDia().getDia();
        this.horario = celulaRequest.getHorario();
        this.dataCadastrado = new Date();
        this.dataAtualizacao = new Date();
        this.cep = celulaRequest.getEndereco().getCep();
        this.logradouro = celulaRequest.getEndereco().getLogradouro();
        this.numero = celulaRequest.getEndereco().getNumero();
        this.complemento = celulaRequest.getEndereco().getComplemento();
        this.bairro = celulaRequest.getEndereco().getBairro();
        this.estado = celulaRequest.getEndereco().getEstado();
        this.uf = celulaRequest.getEndereco().getUf();
    }
}
