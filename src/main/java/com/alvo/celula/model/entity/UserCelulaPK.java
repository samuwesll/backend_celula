package com.alvo.celula.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"cdUser","cdCelula"})
public class UserCelulaPK implements Serializable {

    @Column(name = "cd_user")
    private Long cdUser;

    @Column(name = "cd_celula")
    private Long cdCelula;

}
