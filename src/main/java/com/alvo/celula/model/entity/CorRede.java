package com.alvo.celula.model.entity;

import com.alvo.celula.model.dto.request.CorRedeRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_cor_rede")
public class CorRede implements Serializable {

    @Id
    @Column(name = "cd_cor")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ds_cor_rede")
    private String corRede;

    @Column(name = "cd_cor_css")
    private String stiloCor;

    public CorRede(CorRedeRequest request) {
        this.corRede = request.getNomeCor();
        this.stiloCor = request.getStileCor();
    }
}
