package com.alvo.celula.model.entity;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.request.MembroRequest;
import com.alvo.celula.model.enums.MembroPerfilEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tb_membro")
public class MembroEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_membro")
    private Long id;

    @Column(name = "ds_apelido")
    private String apelido;

    @Column(name = "dt_alteracao")
    private Date dataAlteracao;

    @Column(name = "dt_cadastro")
    private Date dataCadastro;

    @Column(name = "dt_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @Column(name = "nm_membro")
    private String nomeCompleto;

    @Column(name = "nm_celular")
    private String celular;

    @ManyToOne
    @JoinColumn(name = "cd_celula")
    private CelulaEntity celula;

    @Column(name = "ds_perfil")
    private String perfil;

    public MembroEntity(MembroRequest request) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

        this.apelido = request.getApelido();
        this.dataCadastro = new Date();
        this.celular = request.getNmCelular();
        this.nomeCompleto = request.getNomeCompleto();
        try {
            this.dataNascimento = formato.parse(request.getDataNascimento());
        } catch (ParseException e) {
            throw new ExcessaoErro("Formato do campo dataNascimento incorreto", HttpStatus.BAD_REQUEST);
        }
        if (request.getPerfil() != null) {
            String perfil = MembroPerfilEnum.buscarPerfil(request.getPerfil().getNome());
            this.perfil = perfil;
        }

    }
}
