package com.alvo.celula.model.entity;

import com.alvo.celula.model.dto.request.RedeRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tb_rede")
public class RedeEntity implements Serializable {

    @Id
    @Column(name = "cd_rede")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ds_cor", nullable = true)
    private String cor;

    @ManyToOne
    @JoinColumn(name = "cd_church")
    private ChurchEntity church;

    @Column(name = "ds_pastores")
    private String pastores;

    @ManyToOne
    @JoinColumn(name = "cd_cor")
    private CorRede corRede;

    public RedeEntity(RedeRequest request) {
        this.cor = request.getCor();
        this.pastores = request.getPastores();
    }
}
