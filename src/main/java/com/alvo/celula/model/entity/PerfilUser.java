package com.alvo.celula.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_perfil")
public class PerfilUser implements Serializable {

    @Id
    @Column(name = "cd_perfil")
    private Long id;

    @Column(name = "ds_descricao", nullable = false)
    private String descricao;

}
