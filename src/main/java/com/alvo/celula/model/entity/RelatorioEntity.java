package com.alvo.celula.model.entity;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.request.RelatorioCelulaRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tb_relatorio_celula")
public class RelatorioEntity implements Serializable {

    @Id
    @Column(name = "cd_relatorio")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCadastrado;

    @Column(name = "dt_relatorio")
    @Temporal(TemporalType.DATE)
    private Date dataRelatorio;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cd_celula")
    private CelulaEntity celula;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cd_lider")
    private UserEntity lider;

    @Column(name = "vl_kg_amor")
    private Double kgAmor;

    @Column(name = "vl_oferta")
    private Float oferta;

    @Column(name = "ids_membros")
    private String idsMembros;

    @Column(name = "qtd_visitantes")
    private Integer visitantes;

    @Column(name = "ds_observacao")
    private String observacao;

    public RelatorioEntity(RelatorioCelulaRequest request) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

        this.dataCadastrado = new Date();
        this.kgAmor = request.getKgAmor();
        this.visitantes = request.getVisitantes();
        this.observacao = request.getObservaao();
        this.oferta = request.getOferta();

        String ids = "";
        for (Integer i = 0; i < request.getIdsMembros().size(); i++) {
            if (i.equals(0))
                ids = request.getIdsMembros().get(i).toString();
            else ids += ", ".concat(request.getIdsMembros().get(i).toString());
        }
        this.idsMembros = ids;
        try {
            this.dataRelatorio = formato.parse(request.getDataRelatorio());
        } catch (ParseException e) {
            throw new ExcessaoErro("Formato da data incorreto", HttpStatus.BAD_REQUEST);
        }
    }
}
