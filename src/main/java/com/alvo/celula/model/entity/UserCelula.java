package com.alvo.celula.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tb_lider_celula_pk")
@Data
@EqualsAndHashCode(of = "id")
public class UserCelula implements Serializable {

    @EmbeddedId
    private UserCelulaPK id;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "cd_user")
    private UserEntity lideres;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "cd_celula")
    private CelulaEntity celula;

    @ManyToOne
    @JoinColumn(name = "cd_perfil")
    private PerfilUser perfil;

}
