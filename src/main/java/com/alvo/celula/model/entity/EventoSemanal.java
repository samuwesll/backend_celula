package com.alvo.celula.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TB_EVENTO_SEMANAL")
public class EventoSemanal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cd_evento_semanal")
    private Long id;

    @Column(name = "nr_dia_semana")
    private Long diaSemana;

    @Column(name = "ds_evento")
    private String evento;

    @Column(name = "ds_horario")
    private String horario;

    @Column(name = "nm_evento")
    private String nome;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cd_church")
    private ChurchEntity igreja;

}
