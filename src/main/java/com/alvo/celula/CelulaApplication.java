package com.alvo.celula;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class CelulaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CelulaApplication.class, args);
	}

}
