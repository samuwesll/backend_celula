package com.alvo.celula.util;

import com.alvo.celula.security.JwtUser;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class Utils {

    public static JwtUser getPrincipal() {
        SecurityContext context = SecurityContextHolder.getContext();
        JwtUser principal = ((JwtUser) context.getAuthentication().getPrincipal());
        return principal;
    }

}
