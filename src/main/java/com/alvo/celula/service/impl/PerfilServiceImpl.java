package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.PerfilUserDto;
import com.alvo.celula.model.entity.PerfilUser;
import com.alvo.celula.repository.PerfilRepository;
import com.alvo.celula.service.PerfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PerfilServiceImpl implements PerfilService {

    @Autowired
    private PerfilRepository repository;

    @Override
    public List<PerfilUserDto> buscarPerfil() {
        List<PerfilUser> perfil = this.repository.findAll();
        if (perfil.isEmpty()) {
            throw new ExcessaoErro("Não há perfis cadastrados", HttpStatus.BAD_REQUEST);
        }
        List<PerfilUserDto> perfilUserDtos = new ArrayList<>();
        perfil.stream().forEach(perf -> perfilUserDtos.add(new PerfilUserDto(perf)));
        return perfilUserDtos;
    }
}
