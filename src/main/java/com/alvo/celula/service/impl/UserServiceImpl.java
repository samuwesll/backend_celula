package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.PerfilUserDto;
import com.alvo.celula.model.dto.request.UsuarioRequest;
import com.alvo.celula.model.dto.response.UsuarioResponse;
import com.alvo.celula.model.entity.PerfilUser;
import com.alvo.celula.model.entity.RedeEntity;
import com.alvo.celula.model.entity.UserEntity;
import com.alvo.celula.repository.PerfilRepository;
import com.alvo.celula.repository.RedeRepository;
import com.alvo.celula.repository.UserRepository;
import com.alvo.celula.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private PerfilRepository perfilRepository;

    @Autowired
    private RedeRepository redeRepository;

    @Override
    public UsuarioResponse buscaPorLogin(String login) {
        UsuarioResponse user = new UsuarioResponse(repository.findByLogin(login).orElse(null));
        if (user.getId() == null) {
            throw new ExcessaoErro("Login informado não encontrado", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    @Override
    public Optional<UserEntity> buscaPorLoginSeg(String login) {
        return repository.findByLogin(login);
    }

    @Override
    public Long cadastrarUsuario(UsuarioRequest usuarioRequest) {
        Optional<UserEntity> byLogin = this.repository.findByLogin(usuarioRequest.getLogin());
        if (byLogin.isPresent()) {
            throw new ExcessaoErro("login informado j\u00E1 em uso", HttpStatus.BAD_REQUEST);
        }
        UserEntity user = new UserEntity(usuarioRequest);
        RedeEntity rede = this.redeRepository.findById(usuarioRequest.getIdRede()).orElseThrow(() ->
                new ExcessaoErro("rede informada n\u00E3o cadastrada", HttpStatus.BAD_REQUEST));
        PerfilUser perfil = this.perfilRepository.findById(usuarioRequest.getIdPerfil()).orElseThrow(() ->
                new ExcessaoErro("perfil informado n\u00E3o cadastrado", HttpStatus.BAD_REQUEST));

        user.setPerfilUser(perfil);
        user.setRede(rede);

        this.repository.save(user);
        if (user.getId() == null) {
            throw new ExcessaoErro("Erro para cadastrar usuario", HttpStatus.BAD_REQUEST);
        }
        return user.getId();
    }

    @Override
    public UsuarioResponse buscaPorId(Long id) {
        Optional<UserEntity> usuario = this.repository.findById(id);
        if (!usuario.isPresent()) {
            throw new ExcessaoErro("Não há usuario com o id informado", HttpStatus.BAD_REQUEST);
        }
        return new UsuarioResponse(usuario.get());
    }

    @Override
    public List<UsuarioResponse> listarTodos() {
        List<UserEntity> entities = this.repository.findAll();
        List<UsuarioResponse> usuarioResponses = new ArrayList<>();
        if (entities.isEmpty()) {
            throw new ExcessaoErro("Não há nenhum usuario cadastrado", HttpStatus.NOT_FOUND);
        }
        entities.forEach(user -> usuarioResponses.add(new UsuarioResponse(user)));
        return usuarioResponses;
    }

    @Override
    public List<UsuarioResponse> buscarPorPerfil(Long idPerfil) {
        List<UserEntity> entities = this.repository.findByPerfilUserId(idPerfil);
        List<UsuarioResponse> usuarioResponses = new ArrayList<>();
        if (entities.isEmpty()) {
            throw new ExcessaoErro("Não há nenhum supervisor cadastrado", HttpStatus.NOT_FOUND);
        }
        entities.forEach(user -> usuarioResponses.add(new UsuarioResponse(user)));
        return usuarioResponses;
    }

    @Override
    public List<UsuarioResponse> listarPorIdRede(Long idRede) {
        this.redeRepository.findById(idRede).orElseThrow(() ->
                new ExcessaoErro("rede informada não cadastrada", HttpStatus.BAD_REQUEST));

        List<UserEntity> userEntities = this.repository.findByRedeId(idRede);
        if (userEntities.isEmpty()) {
            throw new ExcessaoErro("Não há usuarios vinculado na rede informada", HttpStatus.NOT_FOUND);
        }
        List<UsuarioResponse> responses = new ArrayList<>();
        userEntities.forEach(user -> {
            responses.add(UsuarioResponse.builder()
                    .id(user.getId())
                    .apelido(user.getApelido())
                    .nomeCompleto(user.getNome())
                    .perfil(new PerfilUserDto(user.getPerfilUser()))
                    .build());
        });
        return responses;
    }
}
