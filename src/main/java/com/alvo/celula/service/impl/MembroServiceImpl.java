package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.request.MembroRequest;
import com.alvo.celula.model.dto.response.CargaInicialAppMembros;
import com.alvo.celula.model.dto.response.MembroResponse;
import com.alvo.celula.model.entity.CelulaEntity;
import com.alvo.celula.model.entity.MembroEntity;
import com.alvo.celula.model.enums.MembroPerfilEnum;
import com.alvo.celula.repository.CelulaRepository;
import com.alvo.celula.repository.MembroRepository;
import com.alvo.celula.security.JwtUser;
import com.alvo.celula.service.MembroService;
import com.alvo.celula.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MembroServiceImpl implements MembroService {

    @Autowired
    private MembroRepository membroRepository;

    @Autowired
    private CelulaRepository celulaRepository;

    @Override
    @Transactional
    public Long cadastrar(MembroRequest request) {
        CelulaEntity celulaEntity = celulaRepository.findById(request.getCelulaId()).orElseThrow(() ->
                new ExcessaoErro("Celula informada n\u00E3o cadastra", HttpStatus.BAD_REQUEST));

        MembroEntity entity = new MembroEntity(request);
        entity.setCelula(celulaEntity);

        membroRepository.save(entity);
        return entity.getId();
    }

    @Override
    @Transactional
    public List<MembroResponse> membrosCelula(Long idCelula) {
        this.celulaRepository.findById(idCelula).orElseThrow(() -> new ExcessaoErro("C\u00E9lula informada n\u00E3o encontrada.", HttpStatus.NOT_FOUND));
        List<MembroEntity> membroEntities = this.membroRepository.findByCelulaId(idCelula);

        List<MembroResponse> membroResponseList = new ArrayList<>();

//        Page<MembroEntity> all = this.membroRepository.findAll(PageRequest.of(1, 1));

        for (MembroEntity membro : membroEntities) {
            membroResponseList.add(MembroResponse.converteEntityParaResponse(membro));
        }
        return membroResponseList;
    }

    @Override
    public List<MembroResponse> listarMembros(Pageable pageable) {
        return null;
    }

    @Override
    public void deletar(Long idMembro) {
        MembroEntity membroEntity = this.membroRepository.findById(idMembro).orElseThrow(() ->
                new ExcessaoErro("id do membro informado não cadastro na base", HttpStatus.NOT_FOUND));

        this.membroRepository.delete(membroEntity);
    }

    @Transactional
    @Override
    public List<CargaInicialAppMembros> carregarMembrosPorUser() {
        JwtUser principal = Utils.getPrincipal();
        List<CelulaEntity> celulas = celulaRepository.findByVinculoUserIdCdUserAndDataDesativadoIsNull(principal.getId());

        if (celulas.isEmpty()) {
            throw new ExcessaoErro("N\u00E3o h\u00E1 celulas cadastrada na base para esse usu\u00E1rio", HttpStatus.BAD_REQUEST);
        }

        List<CargaInicialAppMembros> resposeDTO = new ArrayList<>();

        celulas.stream().forEach(c -> {
            List<MembroResponse> lista = this.membrosCelula(c.getId());
            CargaInicialAppMembros cargaMembros = new CargaInicialAppMembros();
            cargaMembros.setIdCelula(c.getId());
            cargaMembros.setMembros(lista);
            resposeDTO.add(cargaMembros);
        });

        return resposeDTO;
    }

    @Override
    @Transactional
    public void editar(MembroRequest request) {
        MembroEntity membroEntity = this.membroRepository.findById(request.getId()).orElseThrow(() ->
            new ExcessaoErro("Membro n\u00E3o encontrado com o id informado", HttpStatus.BAD_REQUEST));
        membroEntity.setDataAlteracao(new Date());

        if (request.getNomeCompleto() != null) {
            membroEntity.setNomeCompleto(request.getNomeCompleto());
        }

        if (request.getNmCelular() != null) {
            membroEntity.setCelular(request.getNmCelular());
        }

        if (request.getApelido() != null) {
            membroEntity.setApelido(request.getApelido());
        }

        if (request.getPerfil() != null) {
            String perfil = MembroPerfilEnum.buscarPerfil(request.getPerfil().getNome());
            membroEntity.setPerfil(perfil);
        }

        if (request.getDataNascimento() != null) {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            try {
                membroEntity.setDataNascimento(formato.parse(request.getDataNascimento()));
            } catch (ParseException e) {
                throw new ExcessaoErro("Formato do campo dataNascimento incorreto", HttpStatus.BAD_REQUEST);
            }
        }

        this.membroRepository.save(membroEntity);
    }
}
