package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.CargaInicialAppItem;
import com.alvo.celula.model.dto.RelatorioMes;
import com.alvo.celula.model.dto.request.RelatorioCelulaRequest;
import com.alvo.celula.model.dto.request.SearchRelatorio;
import com.alvo.celula.model.dto.response.CargaInicialApp;
import com.alvo.celula.model.dto.response.RelatorioDiaResponse;
import com.alvo.celula.model.dto.response.RelatorioResponse;
import com.alvo.celula.model.entity.CelulaEntity;
import com.alvo.celula.model.entity.RelatorioEntity;
import com.alvo.celula.model.entity.UserEntity;
import com.alvo.celula.model.enums.DiaSemanaEnum;
import com.alvo.celula.repository.CelulaRepository;
import com.alvo.celula.repository.RelatorioRepository;
import com.alvo.celula.repository.RelatorioRepositoryCRUD;
import com.alvo.celula.repository.UserRepository;
import com.alvo.celula.service.RelatorioService;
import com.alvo.celula.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class RelatorioServiceImpl implements RelatorioService {

    @Autowired
    private RelatorioRepository relatorioRepository;

    @Autowired
    private CelulaRepository celulaRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RelatorioRepositoryCRUD relatorioCRUD;

    @Override
    @Transactional
    public Long salvar(RelatorioCelulaRequest request) {
        RelatorioEntity entity = new RelatorioEntity(request);

        List<RelatorioEntity> relatorioCadastrado = this.relatorioRepository.findByCelulaIdAndDataRelatorio(request.getIdCelula(), entity.getDataRelatorio());

        if (!relatorioCadastrado.isEmpty()) {
            return null;
        }


        CelulaEntity celulaEntity = celulaRepository.findById(request.getIdCelula())
                .orElseThrow(() -> new ExcessaoErro("Célula não cadastrada", HttpStatus.BAD_REQUEST));

        UserEntity userEntity = userRepository.findById(request.getIdLider())
                .orElseThrow(() -> new ExcessaoErro("Usuario não cadastrado não cadastrado", HttpStatus.BAD_REQUEST));

        entity.setCelula(celulaEntity);
        entity.setLider(userEntity);

        relatorioRepository.saveAndFlush(entity);
        return entity.getId();
    }

    @Override
    public void editar(RelatorioCelulaRequest request, Long id) {
        RelatorioEntity entity = this.relatorioRepository.findById(id).orElseThrow(() ->
                new ExcessaoErro("Id informado não existe na base"));
        entity.setKgAmor(request.getKgAmor());
        entity.setObservacao(request.getObservaao());
        entity.setVisitantes(request.getVisitantes());
        entity.setOferta(request.getOferta());

        String ids = "";
        for (Integer i = 0; i < request.getIdsMembros().size(); i++) {
            if (i.equals(0))
                ids = request.getIdsMembros().get(i).toString();
            else ids += ", ".concat(request.getIdsMembros().get(i).toString());
        }
        entity.setIdsMembros(ids);

        this.relatorioRepository.save(entity);
    }

    @Override
    public List<RelatorioResponse> buscarRelatorios(SearchRelatorio search) {
        if (search.getIdSupervisor() == null) {
            search.setIdSupervisor(Utils.getPrincipal().getId());
        }

        final UserEntity user = userRepository.findById(search.getIdSupervisor()).orElseThrow(() ->
                new ExcessaoErro("Usuario não cadastrado não cadastrado", HttpStatus.BAD_REQUEST));

        List<CelulaEntity> celulas;
        List<RelatorioEntity> relatorio;

        if (search.getFlagSupervisor() == null && Arrays.asList(3l,4l).contains(user.getPerfilUser().getId())) {
            celulas = this.celulaRepository.findByVinculoUserLideresRedeIdAndDataDesativadoIsNull(user.getRede().getId()).stream().collect(Collectors.toList());;
            relatorio = this.relatorioCRUD.buscarRelatorioPorParametrosERede(search.getData(), search.getDataFim(), user.getRede().getId());
        } else {
            celulas = this.celulaRepository.findByVinculoUserIdCdUserAndDataDesativadoIsNull(search.getIdSupervisor());
            relatorio = this.relatorioCRUD.buscarRelatorioPorParametros(search.getData(), search.getDataFim(), search.getIdSupervisor());
        }


        List<RelatorioResponse> relatorios = this.converterEntityParaResponse(search.getData(), celulas, relatorio);

        return relatorios;
    }

    @Override
    public CargaInicialApp cargaInicial() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, 1);

        Long id = Utils.getPrincipal().getId();

        List<CelulaEntity> celulas = this.celulaRepository.findByVinculoUserIdCdUserAndDataDesativadoIsNull(id);

        String dataInicial = formato.format(celulas.get(0).getDataCadastrado());
        String dataFinal = formato.format(cal.getTime());

        List<RelatorioEntity> relatorio = this.relatorioCRUD.buscarRelatorioPorParametros(dataInicial, dataFinal, id);

        CargaInicialApp cargaInicialApp = this.converterEntityParaResponseCargaInicial(dataInicial, formato.format(new Date()), celulas, relatorio);

        return cargaInicialApp;
    }

    private CargaInicialApp converterEntityParaResponseCargaInicial(String dataInicial, String dataFinal, List<CelulaEntity> celulas, List<RelatorioEntity> relatorios) {
        CargaInicialApp carga = new CargaInicialApp();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Date dataInicio = null;
        Date dataFim = null;

        try {
            dataInicio = format.parse(dataInicial);
            dataFim = format.parse(dataFinal);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (CelulaEntity celula : celulas) {
            List<RelatorioEntity> relat = relatorios.stream().filter(r -> r.getCelula().getId() == celula.getId()).collect(Collectors.toList());
            CargaInicialAppItem item = new CargaInicialAppItem();
            item.setIdCelula(celula.getId());
            item.setNomeCelula(celula.getNome());

            Date dataAtual = dataInicio;

            do {
                Calendar cal = Calendar.getInstance();
                cal.setTime(dataAtual);
                String data = format.format(dataAtual);

                try {
                    List<Date> list = qtDiasMes(data, DiaSemanaEnum.buscarEnum(celula.getDiaSemana()));
                    List<RelatorioDiaResponse> dias = new ArrayList<>();
                    int index = 0;
                    for (Date dia : list) {
                        List<RelatorioEntity> lista = relat.stream().filter(r -> r.getDataRelatorio().getTime() == dia.getTime()).collect(Collectors.toList());
                        if (lista.size() == 0) {
                            dias.add(new RelatorioDiaResponse(dia, index));
                        } else {
                            dias.add(new RelatorioDiaResponse(lista.get(0), index));
                        }
                        index++;
                    }
                    RelatorioMes mes = new RelatorioMes(dias);
                    mes.setMesAno(data);
                    final int size = mes.getDias().stream().filter(r -> r.getStatus()).collect(Collectors.toList()).size();
                    if (mes.getDias().size() == size) {
                        mes.setStatus(true);
                    }
                    item.getMeses().add(mes);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                cal.add(Calendar.MONTH, 1);
                dataAtual = cal.getTime();
            } while (dataAtual.getTime() <= dataFim.getTime());

            carga.getCelulas().add(item);
        }
        carga.getCelulas().forEach(cell -> {
            Collections.reverse(cell.getMeses());
        });
        return carga;
    }

    private List<RelatorioResponse> converterEntityParaResponse(String data, List<CelulaEntity> celulas, List<RelatorioEntity> relatorios) {
        List<RelatorioResponse> listaRelatorio = new ArrayList<>();

        for (CelulaEntity celula : celulas) {
            List<RelatorioEntity> relat = relatorios.stream().filter(r -> r.getCelula().getId() == celula.getId()).collect(Collectors.toList());
            RelatorioResponse relatorioResponse = new RelatorioResponse(celula);
            try {
                List<Date> listaDias = this.qtDiasMes(data, DiaSemanaEnum.buscarEnum(celula.getDiaSemana()));
                for (int i = 0; i < listaDias.size(); i++) {
                    int finalI = i;
                    List<RelatorioEntity> lista = relat.stream().filter(r -> r.getDataRelatorio().getTime() == listaDias.get(finalI).getTime()).collect(Collectors.toList());
                    if (lista.size() == 0) {
                        relatorioResponse.getDiasRelatorio().add(new RelatorioDiaResponse(listaDias.get(i), i));
                    } else {
                        relat.stream().forEach(r -> {
                            if (r.getDataRelatorio().getTime() == listaDias.get(finalI).getTime()) {
                                relatorioResponse.getDiasRelatorio().add(new RelatorioDiaResponse(r, finalI));
                            }
                        });
                    }
                };
            } catch (ParseException e) {
                e.printStackTrace();
            }


            relatorioResponse.setOfertas(relatorioResponse.getDiasRelatorio().stream().map(r -> r.getOfertas()).mapToDouble(v -> v.doubleValue()).sum());
            relatorioResponse.setQuilos(relatorioResponse.getDiasRelatorio().stream().map(q -> q.getQuilos()).mapToDouble(v -> v.doubleValue()).sum());
            relatorioResponse.setVisitante(relatorioResponse.getDiasRelatorio().stream().map(vis -> vis.getVisitante()).mapToInt(v -> v.intValue()).sum());
            relatorioResponse.setMembros(relatorioResponse.getDiasRelatorio().stream().map(mem -> mem.getMembros()).max(Comparator.comparing(v -> v)).orElse(0));
            final int size = relatorioResponse.getDiasRelatorio().stream().filter(r -> r.getStatus()).collect(Collectors.toList()).size();
            if (relatorioResponse.getDiasRelatorio().size() == size) {
                relatorioResponse.setStatus(true);
            }

            listaRelatorio.add(relatorioResponse);
        }
        return listaRelatorio;
    }

    private static List<Date> qtDiasMes(String anoEMes, DiaSemanaEnum diaSemanaEnum) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        List<Date> listDias = new ArrayList<>();
        GregorianCalendar gc = new GregorianCalendar();

        String[] data = anoEMes.split("-").clone();
        final Integer ano = Integer.parseInt(data[0]);
        final Integer mes = Integer.parseInt(data[1]);
        YearMonth yearMonthObject = YearMonth.of(ano, mes);
        int qtdDias = yearMonthObject.lengthOfMonth();
        for (int i = 1; i <= qtdDias; i++) {
            gc.setTime(formato.parse(anoEMes + "-" + i));
            if (gc.get(Calendar.DAY_OF_WEEK) == diaSemanaEnum.getDia()) {
                listDias.add(gc.getTime());
            }
        }
        return listDias;
    }

}
