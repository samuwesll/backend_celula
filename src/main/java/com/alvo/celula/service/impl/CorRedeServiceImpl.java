package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.request.CorRedeRequest;
import com.alvo.celula.model.dto.response.CorRedeResponse;
import com.alvo.celula.model.entity.CorRede;
import com.alvo.celula.repository.CorRedeRepository;
import com.alvo.celula.service.CorRedeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CorRedeServiceImpl implements CorRedeService {

    @Autowired
    private CorRedeRepository repository;

    @Override
    public List<CorRedeResponse> listarTodos() {
        List<CorRede> all = repository.findAll();
        List<CorRedeResponse> collect = all.stream().map(CorRedeResponse::new).collect(Collectors.toList());
        return collect;
    }

    @Override
    @Transactional
    public void cadastrarNovaCor(CorRedeRequest request) {
//        if (request.getNomeCor() == null || request.getStileCor() == null) {
//            throw new ExcessaoErro("Preencher os campos necessarios", HttpStatus.BAD_REQUEST);
//        }
        CorRede entity = new CorRede(request);
        repository.save(entity);
    }
}
