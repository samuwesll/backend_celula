package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.request.RedeRequest;
import com.alvo.celula.model.dto.response.RedeResponse;
import com.alvo.celula.model.entity.ChurchEntity;
import com.alvo.celula.model.entity.CorRede;
import com.alvo.celula.model.entity.RedeEntity;
import com.alvo.celula.repository.ChurchRepository;
import com.alvo.celula.repository.CorRedeRepository;
import com.alvo.celula.repository.RedeRepository;
import com.alvo.celula.service.RedeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RedeServiceImpl implements RedeService {

    @Autowired
    private RedeRepository redeRepository;

    @Autowired
    private ChurchRepository churchRepository;

    @Autowired
    private CorRedeRepository corRepository;

    @Override
    @Transactional
    public RedeResponse cadastrar(RedeRequest request) {
        RedeEntity redeEntity = new RedeEntity(request);
        ChurchEntity churchEntity = churchRepository.findById(request.getIdIgreja()).orElseThrow(() ->
                new ExcessaoErro("Igreja não encontrada com o id informado", HttpStatus.BAD_REQUEST));

        CorRede cor = corRepository.findById(request.getIdCor()).orElseThrow(() ->
                new ExcessaoErro("Igreja não encontrada com o id informado", HttpStatus.BAD_REQUEST));
        redeEntity.setChurch(churchEntity);
        redeEntity.setCorRede(cor);
        redeRepository.save(redeEntity);
        RedeResponse redeResponse = new RedeResponse(redeEntity);
        return redeResponse;
    }

    @Override
    public List<RedeResponse> buscarRedes() {
        List<RedeEntity> redes = redeRepository.findAll();
        if (redes.isEmpty()) {
            throw new ExcessaoErro("Não redes cadastradas na base", HttpStatus.NOT_FOUND);
        }
        List<RedeResponse> redeResponses = new ArrayList<RedeResponse>();
        redes.stream().forEach(rede -> redeResponses.add(new RedeResponse(rede)));
        return redeResponses;
    }
}
