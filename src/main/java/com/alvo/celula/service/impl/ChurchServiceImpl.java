package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.ChurchDto;
import com.alvo.celula.model.entity.ChurchEntity;
import com.alvo.celula.model.response.Response;
import com.alvo.celula.repository.ChurchRepository;
import com.alvo.celula.service.ChurchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChurchServiceImpl implements ChurchService {

    @Autowired
    private ChurchRepository repository;

    @Override
    public Long salvarChurch(ChurchDto churchDto) {
        ChurchEntity churchEntity = new ChurchEntity(churchDto);
        repository.save(churchEntity);
        return churchEntity.getId();
    }

    @Override
    public List<ChurchDto> listarChurch() {
        List<ChurchEntity> churchs = repository.findAll();
        if (churchs.isEmpty()) {
            throw new ExcessaoErro("Não há igrejas na base", HttpStatus.BAD_REQUEST);
        }
        List<ChurchDto> churchDtos = new ArrayList<>();
        churchs.stream().forEach(churchEntity -> churchDtos.add(new ChurchDto(churchEntity)));
        return churchDtos;
    }
}
