package com.alvo.celula.service.impl;

import com.alvo.celula.handler.ExcessaoErro;
import com.alvo.celula.model.dto.CelulaDto;
import com.alvo.celula.model.dto.response.CelulaDetalheResponse;
import com.alvo.celula.model.dto.response.CelulaResponse;
import com.alvo.celula.model.dto.request.CelulaRequest;
import com.alvo.celula.model.entity.*;
import com.alvo.celula.repository.*;
import com.alvo.celula.security.JwtUser;
import com.alvo.celula.service.CelulaService;
import com.alvo.celula.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CelulaServiceImpl implements CelulaService {

    @Autowired
    private CelulaRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCelulaRepository userCelulaRepository;

    @Autowired
    private PerfilRepository perfilRepository;

    @Autowired
    private MembroRepository membroRepository;

    @Override
    public List<CelulaResponse> buscarCelulas() {
        List<CelulaEntity> celulas = repository.findAll();
        if (celulas.isEmpty()) {
            throw new ExcessaoErro("N\u00E3o h\u00E1 celulas cadastrada na base", HttpStatus.BAD_REQUEST);
        }
        List<CelulaResponse> celulaResponse = new ArrayList<>();
        celulas.stream().forEach(cel -> celulaResponse.add(new CelulaResponse(cel)));
        return celulaResponse;
    }

    @Override
    @Transactional
    public Long cadastrarCelula(CelulaRequest celulaRequest) {
        CelulaEntity entity = new CelulaEntity(celulaRequest);
        this.repository.save(entity);
        if (entity.getId() == null) {
            throw new ExcessaoErro("Erro ao cadastrar celula", HttpStatus.BAD_REQUEST);
        }
        if (celulaRequest.getIdLideres() != null) {
            for (Long lider : celulaRequest.getIdLideres()) {
                this.associarCelula(entity.getId(), lider, 5l);
            }
        }
        if (celulaRequest.getIdSupers() != null) {
            for (Long sup : celulaRequest.getIdSupers()) {
                this.associarCelula(entity.getId(), sup, 7l);
            }
        }
        return entity.getId();
    }

    @Override
    public void deletarCelula(Long id) {
        CelulaEntity celula = repository.findById(id).orElseThrow(() ->
                new ExcessaoErro("C\u00E9lula n\u00E3o encontrada", HttpStatus.NOT_FOUND));
        repository.delete(celula);
    }

    @Override
    @Transactional
    public void associarCelula(Long idCelula, Long idLider, Long idPerfil) {
        UserEntity usuario = userRepository.findById(idLider).orElseThrow(() ->
                new ExcessaoErro("lider informado n\u00E3o encontrado na base", HttpStatus.BAD_REQUEST));

        PerfilUser perfilEntity = perfilRepository.findById(idPerfil).orElseThrow(() ->
                new ExcessaoErro("perfil informado, n\u00E3o encontrado na base", HttpStatus.BAD_REQUEST));

        UserCelula userCelula = userCelulaRepository.findById(new UserCelulaPK(idLider, idCelula)).orElse(null);
        if (userCelula == null) {
            userCelula = new UserCelula();
            userCelula.setId(new UserCelulaPK(idLider, idCelula));
            userCelula.setLideres(usuario);
            userCelula.setPerfil(perfilEntity);
        } else {
            return;
        }
        userCelulaRepository.save(userCelula);
    }

    @Override
    public List<CelulaResponse> buscarCelulasPorIdLider() {
        JwtUser user = Utils.getPrincipal();
        List<CelulaEntity> celulas = repository.findByVinculoUserIdCdUserAndDataDesativadoIsNull(user.getId());
        if (celulas.isEmpty()) {
            throw new ExcessaoErro("N\u00E3o h\u00E1 celulas cadastrada na base para esse usu\u00E1rio", HttpStatus.BAD_REQUEST);
        }
        List<CelulaResponse> celulaResponse = new ArrayList<>();
        celulas.stream().forEach(cel -> celulaResponse.add(new CelulaResponse(cel)));
        return celulaResponse;
    }

    @Override
    @Transactional
    public CelulaDetalheResponse celulaDetalhe(Long idCelula) {
        CelulaEntity celulaEntity = repository.findById(idCelula).orElseThrow(() -> new ExcessaoErro("C\u00E9lula n\u00E3o encontrada", HttpStatus.NOT_FOUND));

        List<MembroEntity> membrosEntities = membroRepository.findByCelulaId(idCelula);

        CelulaDetalheResponse celulaDetalhe = new CelulaDetalheResponse(celulaEntity, membrosEntities, celulaEntity.getVinculoUser());

        return celulaDetalhe;
    }
}
