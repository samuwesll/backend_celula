package com.alvo.celula.service;

import com.alvo.celula.model.dto.ChurchDto;
import com.alvo.celula.model.response.Response;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ChurchService {

    Long salvarChurch(ChurchDto churchDto);

    List<ChurchDto> listarChurch();

}
