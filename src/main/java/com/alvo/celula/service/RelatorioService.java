package com.alvo.celula.service;

import com.alvo.celula.model.dto.request.RelatorioCelulaRequest;
import com.alvo.celula.model.dto.request.SearchRelatorio;
import com.alvo.celula.model.dto.response.CargaInicialApp;
import com.alvo.celula.model.dto.response.RelatorioResponse;

import java.util.List;

public interface RelatorioService {

    Long salvar(RelatorioCelulaRequest request);

    void editar(RelatorioCelulaRequest request, Long id);

    List<RelatorioResponse> buscarRelatorios(SearchRelatorio search);

    CargaInicialApp cargaInicial();

}
