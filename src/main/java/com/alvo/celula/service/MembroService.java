package com.alvo.celula.service;

import com.alvo.celula.model.dto.request.MembroRequest;
import com.alvo.celula.model.dto.response.CargaInicialAppMembros;
import com.alvo.celula.model.dto.response.MembroResponse;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MembroService {

    Long cadastrar(MembroRequest request);

    List<MembroResponse> membrosCelula(Long idCelula);

    List<MembroResponse> listarMembros(Pageable pageable);

    void deletar(Long idMembro);

    List<CargaInicialAppMembros> carregarMembrosPorUser();

    void editar(MembroRequest request);

}
