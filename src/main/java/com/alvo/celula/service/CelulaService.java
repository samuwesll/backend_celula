package com.alvo.celula.service;

import com.alvo.celula.model.dto.response.CelulaDetalheResponse;
import com.alvo.celula.model.dto.response.CelulaResponse;
import com.alvo.celula.model.dto.request.CelulaRequest;

import java.util.List;


public interface CelulaService {

    List<CelulaResponse> buscarCelulas();

    Long cadastrarCelula(CelulaRequest celulaRequest);

    void deletarCelula(Long id);

    void associarCelula(Long idCelula, Long idLider, Long idPerfil);

    List<CelulaResponse> buscarCelulasPorIdLider();

    CelulaDetalheResponse celulaDetalhe(Long idCelula);
}
