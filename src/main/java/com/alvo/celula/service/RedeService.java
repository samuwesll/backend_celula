package com.alvo.celula.service;

import com.alvo.celula.model.dto.request.RedeRequest;
import com.alvo.celula.model.dto.response.RedeResponse;

import java.util.List;

public interface RedeService {

    RedeResponse cadastrar(RedeRequest request);

    List<RedeResponse> buscarRedes();

}
