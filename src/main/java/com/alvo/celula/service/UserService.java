package com.alvo.celula.service;

import com.alvo.celula.model.dto.request.UsuarioRequest;
import com.alvo.celula.model.dto.response.UsuarioResponse;
import com.alvo.celula.model.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserService {

    UsuarioResponse buscaPorLogin(String login);

    Optional<UserEntity> buscaPorLoginSeg(String login);

    Long cadastrarUsuario(UsuarioRequest usuarioRequest);

    UsuarioResponse buscaPorId(Long id);

    List<UsuarioResponse> listarTodos();

    List<UsuarioResponse> buscarPorPerfil(Long idPergil);

    List<UsuarioResponse> listarPorIdRede(Long idRede);

}
