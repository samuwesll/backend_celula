package com.alvo.celula.service;

import com.alvo.celula.model.dto.PerfilUserDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PerfilService {

    List<PerfilUserDto> buscarPerfil();



}
