package com.alvo.celula.service;

import com.alvo.celula.model.dto.request.CorRedeRequest;
import com.alvo.celula.model.dto.response.CorRedeResponse;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Validated
public interface CorRedeService {

    List<CorRedeResponse> listarTodos();

    void cadastrarNovaCor(@Valid CorRedeRequest request);

}
